<?php 
/**
 * Run on uninstall of mmcontact
 * @package mmcontact
 */


// if uninstall.php is not called by WordPress, die
if (!defined('WP_UNINSTALL_PLUGIN')) {
    die;
}

remove_shortcode('mmc_widget');

