# Multimedia Contact: 

 Adds [mmc_widget] shortcode that adds an audio recorder to a page or post to record audio, preview, and send to the user who owns the post or page. The Audio is saved by extending wordpresses admin-ajax.php module and no cloud service or non-native wordpress code is used.