(function() {
    var mmcontact = document.querySelector("div.mmcontact-container");
    var enableButton = mmcontact.querySelector(".mmcontact-enable"),
        recordButton = mmcontact.querySelector(".mmcontact-record"),
        stopButton = mmcontact.querySelector(".mmcontact-stop"),
        soundClips = mmcontact.querySelector(".mmcontact-recorded"),
        mmcontactSubject = mmcontact.querySelector('#mmcontact-subject'),
        mmcontactName = mmcontact.querySelector('#mmcontact-name'),
        mmcontactTelephone = mmcontact.querySelector('#mmcontact-telephone'),
        mmcontactEmail = mmcontact.querySelector('#mmcontact-email');
    var mediaClipBlob, mediaRecorder;

    enableButton.onclick = function () {
        if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
            console.log('getUserMedia supported.');
            navigator.mediaDevices.getUserMedia(
                {
                    audio: true
                })
                .then(function(stream) {
                    var chunks = [];
                    mediaRecorder = new MediaRecorder(stream);
                    // console.log("mediaRecorder object instantiated");
                    // console.log(mediaRecorder);
                    // mediaRecorder.start();
                    // console.log(mediaRecorder.state);;
                    // console.log("recorder started");
                    // recordButton.style.background = "red";
                    // recordButton.style.color = "black";

                    recordButton.onclick = function() {
                        console.log(stopButton);
                        mediaRecorder.start();
                        console.log(mediaRecorder.state);
                        console.log("recorder started");
                        recordButton.style.background = "red";
                        recordButton.style.color = "black";
                        recordButton.disabled = true;
                        stopButton.disabled = false;
                        console.log(stopButton);
                    }

                    stopButton.onclick = function() {
                        mediaRecorder.stop();
                        console.log("recorder stopped");
                        recordButton.style.background = "";
                        recordButton.style.color = "";
                        recordButton.disabled = false;
                        stopButton.disabled = true;
                    }

                    mediaRecorder.ondataavailable = function(e) {
                        chunks.push(e.data);
                    }

                    mediaRecorder.onstop = function(e) {
                        console.log("mediaRecorder on stopButton event");
                        var clipName = prompt("Enter a name for your sound clip");
                        var clipContainer = document.createElement("article");
                        var clipLabel = document.createElement("p");
                        var audio = document.createElement("audio");
                        var deleteButton = document.createElement("button");
                        var sendButton = document.createElement("button");

                        clipContainer.classList.add("clip");
                        audio.classList.add('audio');
                        audio.setAttribute('controls', '');
                        sendButton.innerHTML = "Send";
                        deleteButton.innerHTML = "Delete";
                        clipLabel.innerHTML = clipName;

                        clipContainer.appendChild(audio);
                        clipContainer.appendChild(clipLabel);
                        clipContainer.appendChild(sendButton);
                        clipContainer.appendChild(deleteButton);
                        soundClips.appendChild(clipContainer);

                        mediaClipBlob = new Blob(chunks, {'type': 'audio/ogg; codecs=opus'});
                        // mediaClipBlob = new Blob(chunks, {'type': 'audio/mp3'});
                        console.log(mediaClipBlob.type);
                        console.log(mediaClipBlob.size);
                        chunks = [];
                        var audioURL = window.URL.createObjectURL(mediaClipBlob);
                        audio.src = audioURL;

                        deleteButton.onclick =  function(e) {
                            var evtTgt = e.target;
                            console.log(evtTgt);
                            var audio = document.querySelector(".audio");
                            // line below needed to prevent memory leak
                            window.URL.revokeObjectURL(audioURL);
                            console.log(audio);
                            evtTgt.parentNode.parentNode.removeChild(evtTgt.parentNode);
                        }

                        sendButton.onclick = function() {
                            var validateForm = function() {
                                var data = new FormData();
                                data.append('subject', mmcontactSubject.value);
                                data.append('name', mmcontactName.value);
                                data.append('phone', mmcontactTelephone.value);
                                data.append('email', mmcontactEmail.value);
                                data.append('file', mediaClipBlob);
                                console.log(mediaClipBlob);
                                // required by wordpress to route request to proper function/method
                                data.append('action', 'mmcontact_voicemail_incoming');
                                return data;
                            };
                            var requestObj = new XMLHttpRequest();
                            // mmcontactAjaxPHP object provided by mmcontact.php with wp_localize_script function
                            requestObj.open("POST", mmcontactAjaxPhp.ajaxurl, true);
                            requestObj.onload = function (e) {
                                // Uploaded
                                console.log('// Uploaded');
                                console.log(e);
                                // var response = JSON.parse(e.currentTarget.responseText);
                            };
                            requestObj.send(validateForm());
                        }
                    }
                })
                .catch(function(err){
                    console.log('The following getUserMedia error occured: ' + err);
                });
        } else {
            console.log('getUserMedia not supported on your browser!');
        }
    }
})();