<?php
/*
Plugin Name: mmcontact (Multimedia Contact)
*/

// Ensure PHP execution is only allowed when it is included as part of the core system
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// BEGIN -- shortcode section
function mmcontact_shortcodes_init()
{
    /*
    add short codes and associated javascript to wordpress
    mmcontact_widget(): [mmc_widget] adds voicemail contact form and buttons to page
    */
    // global $post; // trying to determine if page has shortcode in page so I can selectively run code below
    function mmcontact_widget($atts = [], $content = null)
    {
        // Debuggin code - Not ready to trash it yet
        // $content = '<pre>' . var_export($post, TRUE) . '</pre><br>' . "\n";
        // $content .= <<<'EOD'
        $content = <<<'EOD'
    <div class="mmcontact-container">
        <button class="mmcontact-enable"> Leave Voice Message </button>
        <br/>
        <button class="mmcontact-record"> Record </button>
        <button class="mmcontact-stop" disabled> Stop </button>
        <br/>
        <label for="mmcontact-subject"> Subject </label>
        <input id="mmcontact-subject" type="text" size="128"/>
        <br/>
        <label for="mmcontact-name"> Name </label>
        <input id="mmcontact-name" type="text" size="20"/>
        <br/>
        <label for="mmcontact-telephone"> Phone </label>
        <input id="mmcontact-telephone" type="tel"/>
        <br/>
        <label for="mmcontact-email"> Email </label>
        <input id="mmcontact-email" type="email"/>
        <hr>
        <div class="mmcontact-recorded"></div>
    </div>
EOD;

        return $content;
    }

    // enqueue/register mmcontact.js for inclusion in the footer of the page.
    wp_enqueue_script('mmcontact', plugin_dir_url(__FILE__) . 'public/js/mmcontact.js', $dep = array(), $ver = false, $in_footer = true);

    // localize script *what does this do exactly?
    wp_localize_script('mmcontact', 'mmcontactAjaxPhp', array('ajaxurl'=>admin_url('admin-ajax.php')));

    // ajax add actions
    // hook for non registered users
    add_action('wp_ajax_mmcontact_voicemail_incoming', 'mmcontact_voicemail_incoming');
    // hook for logged in users
    add_action('wp_ajax_nopriv_mmcontact_voicemail_incoming', 'mmcontact_voicemail_incoming');

    // register shortcode [mmc_widget] and local function mmcontact_widget as its hook
    add_shortcode('mmc_widget', 'mmcontact_widget');
}

// hook mmcontact_shortcodes_init function to wp init action
add_action('init', 'mmcontact_shortcodes_init');
// END -- shortcode section

// voice mail ajax post request function
function mmcontact_voicemail_incoming() 
{
    // wp_send_json_success();
    // die();

    // DEBUG / Example code
    // echo var_dump($_POST) . "\n" . var_dump($_FILES) . "\n";

    // // wp may send spurious zero with out die(); - especially bad if returning json
    // die();
    function check_file_is_audio( $tmp ) 
    {
        /**
         * Gets real MIME type and then see if its on allowed list
         * 
         * @param string $tmp : path to file
         */
        // $allowed = array(
        //     'audio/mpeg', 'audio/x-mpeg', 'audio/mpeg3', 'audio/x-mpeg-3', 'audio/aiff', 
        //     'audio/mid', 'audio/x-aiff', 'audio/x-mpequrl','audio/midi', 'audio/x-mid', 
        //     'audio/x-midi','audio/wav','audio/x-wav','audio/xm','audio/x-aac','audio/basic',
        //     'audio/flac','audio/mp4','audio/x-matroska','audio/ogg','audio/s3m','audio/x-ms-wax',
        //     'audio/xm'
        // );
        $allowed = array('audio/ogg');
        
        // check REAL MIME type
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $type = finfo_file($finfo, $tmp );
        finfo_close($finfo);

        write_log("check reald MIME type");
        write_log($type);
        error_log(print_r($type));
        
        // check to see if REAL MIME type is inside $allowed array
        if(! $type) {
            if( in_array($type["type"], $allowed) ) {
                return true;
            } else {
                return false;
            }
        } else {
            // finfo_file returned false on error
            return false;
        }
    }

    $fileErrors = array(
		0 => "There is no error, the file uploaded with success",
		1 => "The uploaded file exceeds the upload_max_files in server settings",
		2 => "The uploaded file exceeds the MAX_FILE_SIZE from html form",
		3 => "The uploaded file uploaded only partially",
		4 => "No file was uploaded",
		6 => "Missing a temporary folder",
		7 => "Failed to write file to disk",
		8 => "A PHP extension stopped file to upload" );

    $upload_dir = wp_upload_dir();
    $upload_path = $upload_dir['basedir'] . "/voicemail/";
    $upload_url = $upload_dir['baseurl'] . "/voicemail/";

    if(!file_exists($upload_path)) {
        mkdir($upload_path);
    }

	$posted_data =  isset( $_POST ) ? $_POST : array();
    $file_data = isset( $_FILES ) ? $_FILES : array();
    write_log( $_FILES );

    // // $fileName = $data["ibenic_file_upload"]["name"];
    $fileName = date('H.i.s') . '.ogg';
    $temp_name = $file_data['file']['tmp_name'];
    $file_size = $file_data['file']['size'];
    $fileError = $file_data['file']['error'];
    $mb = 2 * 1024 * 1024;
    $targetPath = $upload_path;
    $response['filename'] = $fileName;
    $response['file_size'] = $file_size;
    if($fileError > 0) {
        $response['response'] = 'ERROR';
        $response['error'] = $fileErrors[ $fileError ];
    } elseif(file_exists($targetPath . "/" . $fileName)) {
        $response['response'] = 'ERROR';
        $response['error'] = 'File already exists.';
    } elseif(! check_file_is_audio($temp_name)) {
        $response['response'] = 'ERROR';
        $response['error'] = 'File is not an ogg audio file.';
    } elseif($file_size <= $mb) {
    // if(move_uploaded_file($temp_name . "/" . $fileName)) {
    if(move_uploaded_file($temp_name, $upload_path . '/' . $fileName)) {
            $response['response'] = 'SUCCESS';
            $response['url'] = $upload_url . "/" . $fileName;
            $file = pathinfo( $targetPath . "/" . $fileName);
            $response['type'] = $type;
        }
    } else {
        $response['response'] = 'ERROR';
        $response['error'] = 'File is too large. Max file size is 2 MB.';
    }

    echo json_encode( $response );
    die();
}